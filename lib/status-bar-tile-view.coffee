{Disposable} = require 'atom'
path = require 'path'

element_name = 'opz-status-bar' + Date.now()

svg = """
<svg id="opzio-svg" width="18px" height="18px" viewBox="0 0 256 256" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid">
<g fill="#61e4df" stroke="none"><path d="M400 2403 c-30 -9 -57 -20 -60 -23 -3 -4 -14 -10 -25 -14 -45 -14 -165 -145 -165 -180 0 -9 -5 -16 -12 -16 -6 0 -9 -2 -6 -6 3 -3 -1 -26 -8 -51 -20 -68 -20 -1158 0 -1226 7 -25 11 -48 8 -51 -3 -4 1 -6 8 -6 7 0 10 -3 7 -7 -12 -11 34 -76 96 -136 51 -48 72 -61 139 -82 l79 -25 1043 0 c894 0 1050 2 1092 15 138 41 229 125 276 256 22 63 22 69 22 532 l-1 467 36 1 36 2 -35 6 -35 6 -1 105 c0 164 -35 253 -134 347 -25 24 -74 55 -110 70 l-65 28 -1065 2 c-952 1 -1071 0 -1120 -14z m2210 -33 c36 -11 86 -36 112 -55 60 -44 122 -149 138 -230 8 -42 10 -231 8 -625 l-3 -565 -23 -50 c-52 -112 -122 -178 -229 -214 -61 -21 -78 -21 -1105 -23 -977 -2 -1047 -1 -1102 16 -106 32 -187 102 -235 206 l-26 55 -3 593 c-3 546 -1 598 15 654 37 124 154 226 288 251 52 9 308 11 1084 9 978 -2 1018 -3 1081 -22z"/> <path d="M924 1924 c-58 -29 -105 -87 -120 -151 -21 -88 -19 -454 2 -521 24 -73 64 -117 130 -141 63 -24 96 -26 162 -9 59 15 135 88 151 144 6 23 11 138 11 279 0 220 -2 244 -21 285 -42 94 -101 130 -211 130 -45 0 -85 -6 -104 -16z m206 -26 c26 -14 57 -41 75 -66 l30 -44 3 -246 c2 -164 0 -260 -8 -285 -16 -55 -85 -120 -142 -136 -97 -27 -207 19 -248 104 -20 41 -21 58 -20 295 0 226 2 256 19 288 23 44 66 87 101 101 47 19 142 13 190 -11z"/><path d="M1844 1908 c-29 -20 -57 -50 -71 -77 -22 -43 -23 -52 -23 -313 0 -306 3 -321 81 -378 56 -40 102 -53 169 -47 93 8 153 50 191 132 16 35 19 68 19 285 0 135 -4 260 -10 279 -14 50 -48 95 -95 125 -37 23 -52 26 -127 26 -80 0 -89 -2 -134 -32z m223 1 c40 -15 82 -57 104 -104 16 -36 19 -68 19 -290 l0 -250 -30 -51 c-76 -134 -277 -138 -360 -8 l-30 48 0 266 0 266 30 48 c38 60 91 85 177 86 34 0 74 -5 90 -11z"/></g>
</svg>
"""
class StatusBarTileView extends HTMLElement

  init: ->
    @link = document.createElement('a')
    @link.classList.add('inline-block')
    @link.href = 'https://opz.io/'
    @appendChild @link

    @icon = document.createElement('div')
    @icon.setAttribute('id', 'opzio-status-bar')
    @icon.classList.add('inline-block')
    @icon.innerHTML = svg
    @link.appendChild @icon

    @msg = document.createElement('span')
    @msg.classList.add('inline-block')
    @link.appendChild @msg

    @setStatus "initializing..."

  show: ->
    @classList.add(element_name, 'inline-block')
    @classList.remove(element_name, 'hidden')

  hide: ->
    @classList.remove(element_name, 'inline-block')
    @classList.add(element_name, 'hidden')

  destroy: ->
    @tooltip?.dispose()
    @classList = ''

  setStatus: (content) ->
    @msg?.textContent = content or ''

  setTitle: (text) ->
    @tooltip?.dispose()
    @tooltip = atom.tooltips.add this,
      title: text

module.exports = document.registerElement(element_name, prototype: StatusBarTileView.prototype, extends: 'div')
