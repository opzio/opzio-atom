opzio-atom
---------------

Quantify your coding inside GitHub's Atom editor.

Installation
---------------

1. Inside Atom, navigate to `Preferences`/`Settings` -> `Install` and search for `wakatime`.

  ![Project Overview](https://wakatime.com/static/img/ScreenShots/atom-wakatime-install-1.png)

2. Click the `Install` button.

  ![Project Overview](https://wakatime.com/static/img/ScreenShots/atom-wakatime-install-2.png)

3. Click `Settings`, then enter your api key.

4. Use Atom like you normally do and your time will automatically be tracked for you.

5. Visit https://opzio.io to see your logged time.

Configuring
---------------

Settings for atom-wakatime are under `Settings -> Packages -> opzio -> Settings`.

Additional settings are in `$HOME/.opzio.cfg` for [wakatime cli](https://github.com/wakatime/wakatime#configuring).

Troubleshooting
---------------

Atom writes errors to the Atom Developer Console. To check for errors:

1. Turn on the debug checkbox in `Settings -> Packages -> wakatime -> Settings`
2. Inside Atom, go to `View -> Developer -> Toggle Developer Tools`
3. Clear the developer console (the circle button top left of the console)
4. Inside Atom, go to `View -> Developer -> Reload Window`

That will reload all plugins including WakaTime, and you should see the startup process logged in the developer console.
If there are no errors in your developer console after startup and editing a file, then check for errors in your `~/.wakatime.log` file.

Note: Behind a proxy? Configure Atom to use your proxy when installing opzio: <https://github.com/atom/apm#behind-a-firewall>

License & Authors
---------------
This plugin is the authorship of Alan Hamlett and various contributors for https://wakatime.com/. In any way are Opz.io and Wakatime related. Please refer to LICENCE and AUTHORS for details.

If you are only interested in a personal code tracker, Wakatime is better suited for your needs.
